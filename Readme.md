# Local Sitemap Spider

This program creates a sitemap out of a IIS folder.  This is useful for scans using Burpsuite or similar.   Even though Burpsuite can spider a website for subsequent tests, it can't uncover all the files without inside knowledge.   This tool will provide a complete list for a tool like Burpsuite to test against.

## Usage

filePath is the local path to be traversed
rootURL is the top level URL that will be used in the site map output

```
LocalSitemapSpider {filePath} {rootURL}
```


## Example
```
LocalSitemapSpider C:\inetpub\wwwroot\test.com http://test.com"
<a href='http://test.com/test.php'>link</a>
<a href='http://test.com/index.php'>link</a>
<a href='http://test.com/error.log'>link</a>
<a href='http://test.com/access.log'>link</a>
<a href='http://test.com/logo.jpg'>link</a>
<a href='http://test.com/reports/users.pdf'>link</a>
```

