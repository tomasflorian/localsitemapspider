﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace LocalSitemapSpider
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "";
            string rootUrl ="";

            if (args.Length != 2)
            {
                Console.WriteLine(@"Usage: LocalSitemapSpider filePath rootURL\r\nExample: LocalSitemapSpider C:\inetpub\wwwroot\website.com http://website.com");
                return;
            }
            else
            {
                path = args[0];
                rootUrl = args[1];
            }


            foreach (string file in Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories))
            {
                string url = file.Replace(path, rootUrl);
                url = url.Replace(@"\", @"/");

                url = @"<a href='" + url + @"'>link</a>";

                Console.WriteLine(url);
            }

        }

        
    }
}
